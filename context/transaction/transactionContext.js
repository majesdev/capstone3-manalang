import React, { createContext, useReducer } from 'react';
import transactionReducer from './transactionReducer';
import axios from '../../axios';
import {
  TRANSACTION_REQUEST,
  TRANSACTION_REQUEST_FAIL,
  ADD_TRANSACTION,
  UPDATE_CATEGORY,
  DELETE_CATEGORY,
  ACCUMULATE_TOTAL,
  GET_TRANSACTIONS,
} from './transactionTypes';

const TransactionContext = createContext();

export const TransactionProvider = ({ children }) => {
  const initialState = {
    transactions: [],
    total: 0,
    loading: false,
    error: null,
  };

  const [state, dispatch] = useReducer(transactionReducer, initialState);

  const addTransaction = async transaction => {
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };
    try {
      dispatch({ type: TRANSACTION_REQUEST });
      const { data } = await axios.post('/transactions', transaction, config);
      dispatch({ type: ADD_TRANSACTION, payload: data });
      dispatch({ type: ACCUMULATE_TOTAL });
    } catch (error) {
      console.log(error.response.data);
      dispatch({
        type: TRANSACTION_REQUEST_FAIL,
        payload: error.response.data,
      });
    }
  };

  const fetchTransactions = async () => {
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };
    try {
      dispatch({ type: TRANSACTION_REQUEST });
      const { data } = await axios.get('/transactions', config);
      dispatch({ type: GET_TRANSACTIONS, payload: data });
      dispatch({ type: ACCUMULATE_TOTAL });
    } catch (error) {
      console.log(error.response.data);
      dispatch({
        type: TRANSACTION_REQUEST_FAIL,
        payload: error.response.data,
      });
    }
  };

  return (
    <TransactionContext.Provider
      value={{
        ...state,
        transactionLoading: state.loading,
        transactionDispatch: dispatch,
        addTransaction,
        fetchTransactions,
      }}
    >
      {children}
    </TransactionContext.Provider>
  );
};

export default TransactionContext;
