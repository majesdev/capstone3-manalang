export const TRANSACTION_REQUEST = 'TRANSACTION_REQUEST';
export const GET_TRANSACTIONS = 'GET_TRANSACTIONS';
export const ADD_TRANSACTION = 'ADD_TRANSACTION';
export const ACCUMULATE_TOTAL = 'ACCUMULATE_TOTAL';
export const DELETE_TRANSACTION = 'DELETE_TRANSACTION';
export const TRANSACTION_REQUEST_FAIL = 'TRANSACTION_REQUEST_FAIL';
