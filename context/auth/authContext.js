import React, { createContext, useReducer, useEffect } from 'react';
import { useRouter } from 'next/router';
import authReducer from './authReducer';
import axios from '../../axios';
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  INIT_AUTH,
  SET_USER,
  LOGOUT_REQUEST,
  LOGOUT,
} from './authTypes';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const initialState = {
    token: null,
    user: null,
    isAuthenticated: null,
    message: null,
    error: null,
    loading: false,
  };
  const router = useRouter();
  const [state, dispatch] = useReducer(authReducer, initialState);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token !== null) {
      dispatch({ type: INIT_AUTH });
      fetchUserDetails();
    }
  }, []);

  const login = async user => {
    try {
      dispatch({ type: LOGIN_REQUEST });
      const { data } = await axios.post('/users/login', user);
      dispatch({ type: LOGIN_SUCCESS, payload: data });
      router.push('/');
    } catch (error) {
      console.log(error.response.data);
      dispatch({ type: LOGIN_FAILED, payload: error.response.data });
    }
  };

  const fetchUserDetails = async () => {
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };
    try {
      dispatch({ type: LOGIN_REQUEST });
      const { data } = await axios.get('/users/details', config);
      dispatch({ type: SET_USER, payload: data });
    } catch (error) {
      console.log(error);
      dispatch({ type: LOGIN_FAILED, payload: error.response.data });
    }
  };

  const logout = () => {
    dispatch({ type: LOGOUT_REQUEST });
    dispatch({ type: LOGOUT });
    router.push('/login');
  };

  return (
    <AuthContext.Provider
      value={{
        state,
        dispatch,
        login,
        logout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
