import {
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAILED,
  CLEAR_MESSAGE,
  CLEAR_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGOUT_REQUEST,
  LOGOUT,
  SET_USER,
  INIT_AUTH,
} from './authTypes';

const authReducer = (state, action) => {
  switch (action.type) {
    case LOGOUT_REQUEST:
    case LOGIN_REQUEST:
    case SIGNUP_REQUEST:
      return {
        ...state,
        loading: true,
        message: null,
        error: null,
      };

    case SIGNUP_SUCCESS:
      return {
        ...state,
        loading: false,
        message: action.payload.message,
        error: null,
      };
    case LOGIN_SUCCESS:
      localStorage.setItem('token', action.payload.token);
      // localStorage.setItem('user', action.payload.user);
      return {
        ...state,
        loading: false,
        token: action.payload.token,
        isAuthenticated: true,
        message: action.payload.message,
        user: action.payload.user,
      };

    case LOGIN_FAILED:
    case SIGNUP_FAILED:
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      return {
        ...state,
        loading: false,
        user: null,
        token: null,
        isAuthenticated: null,
        message: null,
        error: action.payload.error,
      };

    case LOGOUT:
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      return {
        ...state,
        loading: false,
        user: null,
        token: null,
        isAuthenticated: null,
        message: null,
        error: null,
      };

    case INIT_AUTH:
      return {
        ...state,
        token: localStorage.getItem('token'),
        // user: localStorage.getItem('user'),
        isAuthenticated: true,
      };

    case SET_USER:
      return {
        ...state,
        loading: false,
        user: action.payload.user,
        isAuthenticated: true,
      };

    case CLEAR_MESSAGE:
      return {
        ...state,
        message: null,
        error: null,
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: null,
      };
  }
};

export default authReducer;
