import styled from 'styled-components';
import theme from '../styles/theme';
const { colors, fonts, fontSizes } = theme;

export const Label = styled.label`
  color: ${props => (props.isError ? colors.red : colors.navyBlue)};
`;

export const Input = styled.input`
  border-radius: 4px;
  border: 1px solid ${props => props.borderColor || colors.navyBlue};
  padding: 1rem 0.5rem;
  font-family: ${fonts.Calibre};
  font-size: ${fontSizes.md};
  display: block;
  width: 100%;
  margin-bottom: 1.25rem;

  &:hover {
    border-color: ${colors.purple};
  }
`;

export default Input;
