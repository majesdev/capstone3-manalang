import styled from 'styled-components';
import theme from '../styles/theme';
const { colors, fonts, fontSizes } = theme;

const Card = styled.div`
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  background: ${colors.white};
  border-radius: 6px;
  display: inline-block;
  min-height: max-content;
  padding: 0.75rem 1rem;
  margin-bottom: 0.75rem;
  position: relative;
  width: 100%;
  max-width: 100%;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);

  &:hover {
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  }

  @media screen and (max-width: 1024px) {
    width: 100%;
  }
`;

export default Card;
