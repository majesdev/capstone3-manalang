const theme = {
  colors: {
    navyBlue: '#373B53',
    purple: '#6F52ED',
    black: '#28282B',
    smoke: '#F8F8FB',
    white: '#FFFFFF',
    grey: '#606A86',
    red: '#ff0033',
    lightGreen: '#E6F7E6',
    green: '#239754',
    lightYellow: '#FDF7E0',
    yellow: '#F0A153',
  },

  fonts: {
    Calibre:
      'Calibre, San Francisco, SF Pro Text, -apple-system, system-ui, BlinkMacSystemFont, Roboto, Helvetica Neue, Segoe UI, Arial, sans-serif',
  },

  fontSizes: {
    xs: '0.75rem',
    sm: '1rem',
    md: '1.25rem',
    lg: '1.5rem',
    xl: '1.75rem',
    xxl: '2rem',
    h1: '3rem',
  },
};

export default theme;
