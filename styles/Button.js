import styled from 'styled-components';
import theme from '../styles/theme';
const { colors, fonts, fontSizes } = theme;

const Button = styled.button`
  display: block;
  width: 100%;
  border-radius: 4px;
  padding: 1rem 1.5rem;
  outline: none;
  border: none;
  cursor: pointer;
  background-color: ${props => props.bgColor || colors.purple};
  color: ${props => props.color || colors.white};
  font-family: ${fonts.Calibre};
  font-size: ${fontSizes.md};
  font-weight: bold;
  margin: 1.2rem 0;
  &:hover {
    filter: brightness(90%);
  }
`;

export default Button;
