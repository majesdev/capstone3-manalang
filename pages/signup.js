import React, { useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import styled from 'styled-components';
import Logo from '../components/Logo';
import Image from 'next/image';
import theme from '../styles/theme';
import { SignUpForm } from '../components/User';
import AuthContext from '../context/auth/authContext';
import { INIT_AUTH } from '../context/auth/authTypes';
import Alert from '@material-ui/lab/Alert';
const { colors, fonts, fontSizes } = theme;

const StyledMainContainer = styled.main`
  height: 100vh;
  display: flex;

  @media screen and (max-width: 1024px) {
    align-items: center;
    justify-content: center;
  }

  .display-none-sm {
    @media screen and (max-width: 1024px) {
      display: none;
    }
  }
`;

const StyledSection = styled.section`
  width: 50%;
  height: 100%;
  background-color: ${props => props.backgroundColor || colors.white};
  color: ${props => props.color || colors.grey};
  position: relative;

  @media screen and (max-width: 1024px) {
    flex: 1;
  }
`;

const StyledIntro = styled.p`
  max-width: 470px;
  font-weight: bold;
`;

const StyledContent = styled.div`
  z-index: 10;
  padding: 5rem;
`;

const ImageContainer = styled.div`
  padding: 1.2rem 2rem;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SignUpPage = () => {
  const router = useRouter();
  const { state, dispatch } = useContext(AuthContext);

  useEffect(() => {
    state.isAuthenticated && router.push('/');
  }, [state.isAuthenticated]);

  return (
    <React.Fragment>
      <Head>
        <title>Balance | Sign Up</title>
      </Head>
      <StyledMainContainer>
        <StyledSection
          className="display-none-sm"
          backgroundColor={colors.navyBlue}
        >
          <StyledContent>
            <Logo />
            <h2 className="text-white">Manage. Track. Visualize.</h2>
            <StyledIntro>
              A central hub where you can track your income and expense with
              ease.
            </StyledIntro>
          </StyledContent>
          <ImageContainer>
            <Image
              src="/images/all-charts.svg"
              alt="Manage. Track. Visualize."
              height={632}
              width={425}
            />
          </ImageContainer>
        </StyledSection>
        <StyledSection color={colors.navyBlue}>
          {state.error && <Alert severity="error">{state.error}</Alert>}
          {state.message && <Alert severity="success">{state.message}</Alert>}
          <SignUpForm />
        </StyledSection>
      </StyledMainContainer>
    </React.Fragment>
  );
};

export default SignUpPage;
