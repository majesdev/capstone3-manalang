import React, { useContext, useEffect } from 'react';
import Head from 'next/head';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import { useRouter } from 'next/router';
import LoginForm from '../components/User/LoginForm';
import styled from 'styled-components';
import AuthContext from '../context/auth/authContext';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles(theme => ({
  input: {
    margin: theme.spacing(2, 0, 3),
  },

  alert: {
    position: 'absolute',
    top: '0',
    width: '50%',
    left: '50%',
    transform: 'translateX(-50%)',
    marginTop: '10px',
  },
}));

const LoginLayout = styled.div`
  min-height: 100vh;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

const Login = () => {
  const classes = useStyles();
  const { state } = useContext(AuthContext);
  const router = useRouter();

  useEffect(() => {
    state.isAuthenticated && router.push('/');
  }, [state.isAuthenticated]);
  return (
    <React.Fragment>
      <Head>
        <title>Balance | Login</title>
      </Head>
      {state.error && (
        <Alert className={classes.alert} severity="error">
          {state.error}
        </Alert>
      )}
      <LoginLayout>
        <LoginForm />
      </LoginLayout>
    </React.Fragment>
  );
};

export default Login;
