import React, { useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Sidebar from '../components/Sidebar';
import styled from 'styled-components';
import theme from '../styles/theme';
import Layout from '../styles/Layout';
import TransactionContext from '../context/transaction/transactionContext';
import LineChart from '../components/LineChart';

const { colors } = theme;

const Title = styled.h1`
  color: ${colors.purple};
  padding: 2rem 0;
  text-align: center;
`;

const BudgetTrendPage = () => {
  const { transactions, fetchTransactions } = useContext(TransactionContext);
  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem('token') === null) {
      router.push('/login');
    } else {
      fetchTransactions();
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Budget Trend | B.</title>
      </Head>
      <Layout>
        <Sidebar />
        <div className="container">
          <Title className="big-heading">Budget Trend</Title>
          <LineChart rawData={transactions} />
        </div>
      </Layout>
    </React.Fragment>
  );
};

export default BudgetTrendPage;
