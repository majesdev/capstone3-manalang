import React, { useContext, useEffect, useState } from 'react';
import Sidebar from '../../components/Sidebar';
import Head from 'next/head';
import BarChart from '../../components/BarChart';
import TransactionContext from '../../context/transaction/transactionContext';
import styled from 'styled-components';
import theme from '../../styles/theme';
import Layout from '../../styles/Layout';

const { colors } = theme;

const Title = styled.h1`
  color: ${colors.purple};
  padding: 2rem 0;
  text-align: center;
`;

const MonthlyIncomePage = () => {
  const { transactions, fetchTransactions } = useContext(TransactionContext);
  const [incomes, setIncome] = useState([]);

  useEffect(() => {
    fetchTransactions();
    setIncome(
      transactions.filter(transaction => transaction.type === 'Income')
    );
  }, [transactions]);

  return (
    <React.Fragment>
      <Head>
        <title>Monthly Income Chart</title>
      </Head>
      <Layout>
        <Sidebar />
        <div className="container">
          <Title className="big-heading">Monthly Income</Title>
          <BarChart rawData={incomes} label="Monthly Income" />
        </div>
      </Layout>
    </React.Fragment>
  );
};

export default MonthlyIncomePage;
