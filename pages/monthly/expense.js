import React, { useContext, useEffect, useState } from 'react';
import Head from 'next/head';
import BarChart from '../../components/BarChart';
import Sidebar from '../../components/Sidebar';
import TransactionContext from '../../context/transaction/transactionContext';
import styled from 'styled-components';
import theme from '../../styles/theme';
import Layout from '../../styles/Layout';

const { colors } = theme;

const Title = styled.h1`
  color: ${colors.purple};
  padding: 2rem 0;
  text-align: center;
`;

const MonthlyExpensePage = () => {
  const { transactions, fetchTransactions } = useContext(TransactionContext);
  const [expenses, setExpenses] = useState([]);

  useEffect(() => {
    fetchTransactions();
    setExpenses(
      transactions.filter(transaction => transaction.type === 'Expense')
    );
  }, [transactions]);

  return (
    <React.Fragment>
      <Head>
        <title>Monthly Expense Chart</title>
      </Head>
      <Layout>
        <Sidebar />
        <div className="container">
          <Title className="big-heading">Monthly Expense</Title>
          <BarChart rawData={expenses} label="Monthly Expense" />
        </div>
      </Layout>
    </React.Fragment>
  );
};

export default MonthlyExpensePage;
