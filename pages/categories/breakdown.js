import React, { useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Sidebar from '../../components/Sidebar';
import Layout from '../../styles/Layout';
import PieChart from '../../components/PieChart';
import styled from 'styled-components';
import theme from '../../styles/theme';
import CategoryContext from '../../context/category/categoryContext';
import TransactionContext from '../../context/transaction/transactionContext';

const { colors } = theme;

const Title = styled.h1`
  color: ${colors.purple};
  padding: 2rem 0;
  text-align: center;
`;

const CategoryBreakdownPage = () => {
  const { categories, fetchCategories } = useContext(CategoryContext);
  const { transactions, fetchTransactions } = useContext(TransactionContext);

  useEffect(() => {
    if (localStorage.getItem('token') !== null) {
      fetchCategories();
      fetchTransactions();
    } else {
      router.push('/login');
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Category Breakdown | B.</title>
      </Head>
      <Layout>
        <Sidebar />
        <div className="container">
          <Title className="big-heading">Category Breakdown</Title>
          <PieChart categories={categories} transactions={transactions} />
        </div>
      </Layout>
    </React.Fragment>
  );
};

export default CategoryBreakdownPage;
