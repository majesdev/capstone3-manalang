import React from 'react';
import Head from 'next/head';
import Next from 'next/link';

const AboutPage = () => {
  return (
    <React.Fragment>
      <Head>
        <title>About Balance.</title>
      </Head>
      <div className="container">
        <h1 className="big-heading">Balance.</h1>
        <h2>Manage. Track. Visualize.</h2>
        <p>
          A central hub where you can track your income and expense with ease.
        </p>
        <p>
          Our Capstone 3 Project in Zuitt. A Budget Tracker where you can create
          your Transactions and Visualize it using charts and graphs/
        </p>
        <p>&copy; 2021 James Manalang</p>
        <Next href="/">
          <a>Home</a>
        </Next>
      </div>
    </React.Fragment>
  );
};

export default AboutPage;
