import React, { useState, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import styled from 'styled-components';
import { useForm } from 'react-hook-form';
import AuthContext from '../../context/auth/authContext';
import Link from 'next/link';
import Button from '../../styles/Button';
import { Input, Label } from '../../styles/Input';
import axios from '../../axios';
import {
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAILED,
  CLEAR_MESSAGE,
} from '../../context/auth/authTypes';

const useStyles = makeStyles(theme => ({
  alert: {
    marginTop: theme.spacing(3),
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const FormContainer = styled.div`
  max-width: 500px;
  padding: 2rem;
  margin: 5rem auto;
`;

const InputBox = styled.div`
  margin: 1rem 0;
`;

const GroupInputBox = styled.div`
  display: flex;
  align-items: center;
  margin: 1rem 0;

  & > div {
    flex: 1;
  }
`;

const Title = styled.h1`
  margin-bottom: 2rem;
`;

const Subtitle = styled.h2`
  font-size: 2.4rem;
  text-transform: uppercase;
  margin-bottom: 1rem;
  color: #606a86;
`;

const SignInLink = styled.p`
  text-align: center;
`;

const SignUpForm = () => {
  const classes = useStyles();
  const { state, dispatch } = useContext(AuthContext);
  const { register, handleSubmit, errors, reset, watch } = useForm();

  console.log(errors);

  const onSubmit = async data => {
    dispatch({ type: CLEAR_MESSAGE });
    try {
      dispatch({ type: SIGNUP_REQUEST });
      const res = await axios.post('/users/register', data);
      dispatch({ type: SIGNUP_SUCCESS, payload: res.data });
      reset();
    } catch (error) {
      console.log(error.response.data);
      dispatch({ type: SIGNUP_FAILED, payload: error.response.data });
    }
  };

  return (
    <FormContainer>
      <Subtitle className="">Start For Free</Subtitle>
      <Title className="mid-heading">Sign up to Balance.</Title>
      <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
        <GroupInputBox>
          <div className="mr-1">
            <TextField
              name="firstName"
              variant="outlined"
              fullWidth
              id="firstName"
              label="First Name"
              autoFocus
              inputRef={register({
                required: 'Please provide a first name',
              })}
              error={errors.firstName ? true : false}
              helperText={errors.firstName ? errors.firstName.message : ''}
            />
          </div>
          <div>
            <TextField
              variant="outlined"
              fullWidth
              id="lastName"
              label="Last Name"
              name="lastName"
              inputRef={register({
                required: 'Please provide a last name',
              })}
              error={errors.lastName ? true : false}
              helperText={errors.lastName ? errors.lastName.message : ''}
            />
          </div>
        </GroupInputBox>
        <InputBox>
          <TextField
            variant="outlined"
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            inputRef={register({
              required: 'Please provide an email address',
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: 'Invalid email address',
              },
            })}
            error={errors.email ? true : false}
            helperText={errors.email ? errors.email.message : ''}
          />
        </InputBox>
        <InputBox>
          <TextField
            variant="outlined"
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            inputRef={register({
              required: 'Please provide a password',
              minLength: {
                value: 8,
                message: 'Password must be at least 8 characters',
              },
            })}
            error={errors.password ? true : false}
            helperText={errors.password ? errors.password.message : ''}
          />
        </InputBox>
        <InputBox>
          <TextField
            variant="outlined"
            fullWidth
            name="confirmPassword"
            label="Confirm Password"
            type="password"
            id="confirmPassword"
            inputRef={register({
              required: 'Please confirm your password',
              validate: value =>
                value === watch('password') || 'Passwords do not match',
            })}
            error={errors.confirmPassword ? true : false}
            helperText={
              errors.confirmPassword ? errors.confirmPassword.message : ''
            }
          />
        </InputBox>
        <Button disabled={state.loading ? true : false} type="submit">
          Sign Up
        </Button>
        <SignInLink>
          Already have an account?{' '}
          <Link href="/login">
            <a>Login</a>
          </Link>
        </SignInLink>
      </form>
    </FormContainer>
  );
};

export default SignUpForm;
