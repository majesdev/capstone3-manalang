import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Button from '../../styles/Button';
import AuthContext from '../../context/auth/authContext';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Spinner from '../Spinner';

const useStyles = makeStyles(theme => ({
  input: {
    margin: theme.spacing(2, 0, 3),
  },
}));

const Title = styled.h1`
  margin-bottom: 2rem;
  text-align: center;
`;

const Subtitle = styled.h2`
  font-size: 4.8rem;
  margin-bottom: 1rem;
  color: #606a86;
  text-align: center;

  .logo {
    color: #6f52ed;
    font-weight: bold;
  }
`;

const SignInLink = styled.p`
  text-align: center;
`;

const SignIn = () => {
  const { state, login } = useContext(AuthContext);
  const classes = useStyles();
  const router = useRouter();
  const { register, handleSubmit, errors, reset } = useForm();

  const onSubmit = data => {
    console.log(data);
    login(data);
    reset();
  };

  return (
    <React.Fragment>
      <h1 className="big-heading">
        Welcome to <span className="logo">Balance.</span>
      </h1>
      <h2 className="mid-heading">Login your account</h2>
      <Container maxWidth="sm">
        <form onSubmit={handleSubmit(onSubmit)}>
          <TextField
            className={classes.input}
            variant="outlined"
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            inputRef={register({
              required: 'Please provide an email address',
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: 'Invalid email address',
              },
            })}
            error={errors.email ? true : false}
            helperText={errors.email ? errors.email.message : ''}
          />

          <TextField
            variant="outlined"
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            inputRef={register({
              required: 'Please provide a password',
              minLength: {
                value: 8,
                message: 'Password must be at least 8 characters',
              },
            })}
            error={errors.password ? true : false}
            helperText={errors.password ? errors.password.message : ''}
          />
          <Button disabled={state.loading} type="submit">
            {state.loading ? <Spinner /> : 'Login'}
          </Button>
        </form>
      </Container>
      <SignInLink>
        Don't have an account?{' '}
        <Link href="/signup">
          <a>Sign Up</a>
        </Link>
      </SignInLink>
    </React.Fragment>
  );
};

export default SignIn;
