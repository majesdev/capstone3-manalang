import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';

const CategoryOption = ({ categories }) => {
  return (
    <React.Fragment>
      {categories.map(category => (
        <MenuItem key={category._id} value={category._id}>
          {category?.name}
        </MenuItem>
      ))}
    </React.Fragment>
  );
};

export default CategoryOption;
