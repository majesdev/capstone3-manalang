import React, { useEffect } from 'react';
import styled from 'styled-components';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import TrendingDownIcon from '@material-ui/icons/TrendingDown';
import theme from '../../styles/theme';
import axios from '../../axios';

const { colors, fonts, fontSizes } = theme;

const StyledTransactionItem = styled.div`
  display: flex;
  align-items: center;
  padding: 1rem 0.5rem;
  background: #fff;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  margin: 0.625rem 0;
  /* color: ${colors.green}; */
`;

const DescriptionBox = styled.div`
  flex: 1;
`;

const Description = styled.h3`
  font-weight: 500;
  font-size: ${fontSizes.sm};
`;

const Date = styled.p`
  font-weight: 300;
  font-size: ${fontSizes.xs};
  color: ${colors.grey};
`;

const Amount = styled.span`
  font-size: ${fontSizes.lg};
  color: ${props => props.color};
  margin-left: auto;
`;

const IconBox = styled.div`
  background-color: ${props => props.bgColor};
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 6px;
  padding: 0.5rem;
  margin-right: 1rem;
`;

const TransactionItem = ({ description, amount, createdOn, type }) => {
  const typeColor = type === 'Income' ? colors.green : colors.yellow;
  const boxColor = type === 'Income' ? colors.lightGreen : colors.lightYellow;
  return (
    <StyledTransactionItem>
      <IconBox bgColor={boxColor}>
        {type === 'Income' ? (
          <TrendingUpIcon style={{ color: typeColor }} />
        ) : (
          <TrendingDownIcon style={{ color: typeColor }} />
        )}
      </IconBox>
      <DescriptionBox>
        <Description>{description}</Description>
        <Date>{createdOn}</Date>
      </DescriptionBox>
      <div>
        <Amount color={typeColor}>
          {type === 'Income' ? '+ ' : '- '}&#8369;
          {Math.abs(amount).toLocaleString()}
        </Amount>
      </div>
    </StyledTransactionItem>
  );
};

export default TransactionItem;
