import React, { useState, useContext, useEffect } from 'react';
import { useRouter } from 'next/router';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CategoryContext from '../../context/category/categoryContext';
import MenuItem from '@material-ui/core/MenuItem';
import CategoryOption from './CategoryOption';
import Button from '../../styles/Button';

import styled from 'styled-components';

const TextFieldWrapper = styled.div`
  width: 100%;
  margin-bottom: 1rem;
`;

const FormControlWrapper = styled.div`
  width: 100%;
  margin-bottom: 1rem;
`;

const StyledMenuItem = styled(MenuItem)`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const TransactionForm = ({ addTransaction }) => {
  const router = useRouter();
  const { categories, fetchCategories } = useContext(CategoryContext);
  const [transactionType, setTransactionType] = useState('');
  const [description, setDescription] = useState('');
  const [amount, setAmount] = useState('');
  const [category, setCategory] = useState('');

  useEffect(() => {
    fetchCategories();
  }, []);

  const handleSubmit = e => {
    e.preventDefault();
    addTransaction({
      type: transactionType,
      category,
      description,
      amount,
    });

    resetValues();
  };

  const resetValues = () => {
    setTransactionType('');
    setDescription('');
    setAmount('');
    setCategory('');
  };

  return (
    <form style={{ width: '100%' }} onSubmit={handleSubmit}>
      <FormControlWrapper>
        <FormControl fullWidth variant="outlined">
          <InputLabel id="transactionTypeLabel">Transaction Type</InputLabel>
          <Select
            labelId="transactionTypeLabel"
            id="transactionType"
            value={transactionType}
            onChange={e => {
              setTransactionType(e.target.value);
              setCategory('');
            }}
            label="Transaction Type"
          >
            <MenuItem value="Income">Income</MenuItem>
            <MenuItem value="Expense">Expense</MenuItem>
          </Select>
        </FormControl>
      </FormControlWrapper>
      <FormControlWrapper>
        <FormControl fullWidth variant="outlined">
          <InputLabel id="categoryLabel">Category</InputLabel>
          <Select
            labelId="categoryLabel"
            id="category"
            value={category}
            onChange={e => setCategory(e.target.value)}
            label="Category"
          >
            <MenuItem value="" onClick={() => router.push('/categories')}>
              Add Category
            </MenuItem>
            {categories
              .filter(category => category.type === transactionType)
              .map(category => (
                <StyledMenuItem key={category._id} value={category._id}>
                  <span>{category.name}</span>{' '}
                  <span style={{ marginLeft: 'auto' }}>({category.type})</span>
                </StyledMenuItem>
              ))}
          </Select>
        </FormControl>
      </FormControlWrapper>
      <TextFieldWrapper>
        <TextField
          fullWidth
          label="Description"
          variant="outlined"
          onChange={e => setDescription(e.target.value)}
          value={description}
          name="description"
          type="text"
        />
      </TextFieldWrapper>
      <TextFieldWrapper>
        <TextField
          fullWidth
          variant="outlined"
          onChange={e =>
            transactionType === 'Expense'
              ? setAmount(-Math.abs(e.target.value))
              : setAmount(Math.abs(e.target.value))
          }
          value={amount}
          name="amount"
          type="number"
          label="Amount"
        />
      </TextFieldWrapper>
      <Button type="submit">Add Transaction</Button>
    </form>
  );
};

export default TransactionForm;
