import { useState, useEffect, Fragment } from 'react';
import { Line } from 'react-chartjs-2';
import theme from '../styles/theme';
import Moment from 'moment';
const { colors } = theme;

const LineChart = ({ rawData }) => {
  const [months, setMonths] = useState([]);
  const [amount, setAmount] = useState([]);

  useEffect(() => {
    console.log(rawData);

    setAmount(rawData.map(transaction => Math.abs(transaction.amount)));
    setMonths(
      rawData.map(transaction =>
        Moment(transaction.createdOn).format('MMM Do YY')
      )
    );
  }, [rawData]);

  const data = {
    labels: months,
    datasets: [
      {
        label: 'Budget Trend',
        borderColor: colors.purple,
        borderWidth: 3,
        hoverBorderColor: 'black',
        data: amount,
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return (
    <Fragment>
      <Line data={data} options={options} />
    </Fragment>
  );
};

export default LineChart;
