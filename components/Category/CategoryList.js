import React, { useContext, useEffect } from 'react';
import CategoryItem from './CategoryItem';
import Icon from '@material-ui/core/Icon';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Tag from '../../styles/Tag';
import IconButton from '@material-ui/core/IconButton';
import CategoryContext from '../../context/category/categoryContext';
import Spinner from '../Spinner';

const useStyles = makeStyles({
  table: {
    width: '100%',
  },
});

const CategoryList = () => {
  const {
    categories,
    setCurrent,
    fetchCategories,
    loading,
    deleteCategory,
  } = useContext(CategoryContext);
  const classes = useStyles();
  useEffect(() => {
    fetchCategories();
  }, []);
  return loading ? (
    <Spinner />
  ) : (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Category Name</TableCell>
            <TableCell align="center">Category Type</TableCell>
            <TableCell align="center">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {categories?.map(category => (
            <TableRow key={category.name}>
              <TableCell align="left" component="th" scope="row">
                {category?.name}
              </TableCell>

              <TableCell align="center">
                <Tag
                  style={{ margin: '0 auto' }}
                  isIncome={category.type === 'Income' ? true : false}
                >
                  {category?.type}
                </Tag>
              </TableCell>
              <TableCell component="th" align="center" scope="row">
                <IconButton onClick={() => setCurrent(category)}>
                  <Icon>edit</Icon>
                </IconButton>
                <IconButton onClick={() => deleteCategory(category._id)}>
                  <Icon>delete</Icon>
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default CategoryList;
