import React, { useState, useEffect, useContext } from 'react';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '../../styles/Button';
import styled from 'styled-components';
import CategoryContext from '../../context/category/categoryContext';

const TextFieldWrapper = styled.div`
  flex: 1;
  margin-right: 1.25rem;
`;

const FormControlWrapper = styled.div`
  /* flex: 1; */
`;

const CategoryForm = () => {
  const { addCategory, updateCategory, current, clearCurrent } = useContext(
    CategoryContext
  );
  const [category, setCategory] = useState({
    name: '',
    type: 'Income',
  });

  const { name, type } = category;

  useEffect(() => {
    if (current !== null) {
      setCategory(current);
    } else {
      setCategory({
        name: '',
        type: 'Income',
      });
    }
  }, [current]);

  const handleChange = e => {
    setCategory({ ...category, [e.target.name]: e.target.value });
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (current === null) {
      addCategory(category);
    } else {
      updateCategory(category);
    }

    clearAll();
  };

  const clearAll = () => {
    setCategory({
      name: '',
      type: 'Income',
    });
    clearCurrent();
  };

  return (
    <form onSubmit={handleSubmit}>
      <Box display="flex" justifyContent="space-between">
        <TextFieldWrapper>
          <TextField
            fullWidth
            m={2}
            key="categoryName"
            variant="outlined"
            label="Category Name"
            onChange={handleChange}
            value={name}
            name="name"
            type="text"
            placeholder="Salary, Meralco, Netflix, etc. "
          />
        </TextFieldWrapper>
        <FormControlWrapper>
          <FormControl variant="outlined">
            <InputLabel id="categoryTypeLabel">Type</InputLabel>
            <Select
              labelId="categoryTypeLabel"
              id="categoryType"
              value={type}
              onChange={handleChange}
              name="type"
              label="Type"
            >
              <MenuItem value="Income">Income</MenuItem>
              <MenuItem value="Expense">Expense</MenuItem>
            </Select>
          </FormControl>
        </FormControlWrapper>
      </Box>
      <Button type="submit">{current ? 'Save' : 'Add Category'}</Button>
    </form>
  );
};

export default CategoryForm;
