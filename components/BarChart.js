import { useState, useEffect, Fragment } from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';
import theme from '../styles/theme';
import styled from 'styled-components';

const BarContainer = styled.div`
  min-height: 600px;
  max-width: 1200px;
  margin: 0 auto;
`;

const { colors } = theme;

const BarChart = ({ rawData, label }) => {
  const [months, setMonths] = useState([
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ]);
  const [amountPerMonth, setAmountPerMonth] = useState([]);

  useEffect(() => {
    setAmountPerMonth(
      months.map(month => {
        let amount = 0;

        rawData.forEach(element => {
          if (moment(element.createdOn).format('MMM') === month) {
            amount += Math.abs(parseInt(element.amount));
          }
        });
        return amount;
      })
    );
  }, [rawData]);

  const data = {
    labels: months,
    datasets: [
      {
        label: label,
        backgroundColor:
          label === 'Monthly Income' ? colors.lightGreen : colors.lightYellow,
        borderColor: label === 'Monthly Income' ? colors.green : colors.yellow,
        borderWidth: 1,
        hoverBackgroundColor:
          label === 'Monthly Income' ? colors.green : colors.yellow,
        hoverBorderColor:
          label === 'Monthly Income' ? colors.lightGreen : colors.lightYellow,
        data: amountPerMonth,
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return (
    <BarContainer>
      <Bar data={data} options={options} />
    </BarContainer>
  );
};

export default BarChart;
